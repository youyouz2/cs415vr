# TODO
- [x] add end game screen that displays final score when sword sliced a grenade
- [x] slice objects with respect to the cut direction. currently is fixed to cut on X-axis. can be changed at sword blueprint -> slice procedural mesh -> plane normal. https://www.youtube.com/watch?v=oIdKxYYQBdw this may help
- [x] make objects fly upward when created
- [x] maintain a pool of sliceable objects and generate randomly
- [x] score system UI + start/end screen

## not very important TODO
- [x] save some music and its tempo can change the objects generation speed
- [x] find more mesh for sliceable objects
- [x] find another mesh for subsituting sword to slice objects
- [ ] find some background material to make the environment looks better

### Links
for setup the local env (first 7 mins): https://www.youtube.com/watch?v=DiGh6MxDFds&list=LL&index=2&t=883s&pp=gAQBiAQB (don't need Visual Studio)
